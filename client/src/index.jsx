import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import dataClient from './apis/dataClient';
import './custom_bootstrap.scss';
import App from './components/App';

const store = configureStore();
render(
  <ApolloProvider store={store} client={dataClient}>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root'),
);

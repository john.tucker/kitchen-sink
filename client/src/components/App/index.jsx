import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Lights from './Lights';
import Frame from './Frame';
import Menu from './Menu';
import Home from './Home';
import NoMatch from './NoMatch';

const App = () => (
  <Frame>
    <Route path="/" component={Menu} />
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/lights" component={Lights} />
      <Route component={NoMatch} />
    </Switch>
  </Frame>
);
export default App;

import React from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './index.css';

const Menu = ({ location: { pathname } }) => (
  <ul>
    <li className={pathname === '/' ? styles.active : null}><Link to="/">Home</Link></li>
    <li className={pathname === '/lights' ? styles.active : null}><Link to="/lights">Lights</Link></li>
  </ul>
);
Menu.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};
export default Menu;

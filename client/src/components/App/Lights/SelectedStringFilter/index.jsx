import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import * as fromSelectedFilters from '../../../../ducks/selectedFilters';
import { DISPLAY_NAME_BY_ID } from '../../../../strings';
import View from './View';

const SelectedStringFilter = ({ filterName, option, removeSelectedStringFilter }) => (
  <View
    filterName={DISPLAY_NAME_BY_ID[filterName]}
    option={option}
    removeSelectedStringFilter={removeSelectedStringFilter}
  />
);
SelectedStringFilter.propTypes = {
  filterName: PropTypes.string.isRequired,
  option: PropTypes.string.isRequired,
  removeSelectedStringFilter: PropTypes.func.isRequired,
};
export default connect(
  null, {
    removeSelectedStringFilter: fromSelectedFilters.removeSelectedStringFilter,
  },
)(SelectedStringFilter);

import React from 'react';
import { PropTypes } from 'prop-types';
import { Button } from 'reactstrap';

const View = ({
  filterName,
  option,
  removeSelectedStringFilter,
}) => (
  <Button
    color="primary"
    onClick={() => removeSelectedStringFilter({ name: filterName, option })}
  >{filterName} - {option}</Button>
);
View.propTypes = {
  filterName: PropTypes.string.isRequired,
  option: PropTypes.string.isRequired,
  removeSelectedStringFilter: PropTypes.func.isRequired,
};
export default View;

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import * as fromLightsOffset from '../../../../ducks/lightsOffset';
import { MAX_LIGHTS } from '../../../../strings';

const Pager = ({ lightsOffset, setLightsOffset, totalCount }) => {
  const activePage = (Math.floor((lightsOffset + 1) / MAX_LIGHTS)) + 1;
  const lastPage = Math.floor(totalCount / MAX_LIGHTS) + 1;
  return (
    <Pagination>
      <PaginationItem
        disabled={activePage === 1}
        onClick={() => {
          if (activePage === 1) return;
          setLightsOffset(MAX_LIGHTS * (activePage - 2));
        }}
      >
        <PaginationLink previous />
      </PaginationItem>
      {
        activePage !== 1 &&
        <PaginationItem
          onClick={() => setLightsOffset(0)}
        >
          <PaginationLink>
            1
          </PaginationLink>
        </PaginationItem>
      }
      {
        activePage - 2 > 1 &&
        <PaginationItem disabled>
          <PaginationLink>
            ...
          </PaginationLink>
        </PaginationItem>
      }
      {
        activePage - 1 > 1 &&
        <PaginationItem
          onClick={() => setLightsOffset(MAX_LIGHTS * (activePage - 2))}
        >
          <PaginationLink>
            {(activePage - 1).toString()}
          </PaginationLink>
        </PaginationItem>
      }
      <PaginationItem active>
        <PaginationLink>
          {activePage.toString()}
        </PaginationLink>
      </PaginationItem>
      {
        activePage + 1 < lastPage &&
        <PaginationItem
          onClick={() => setLightsOffset(MAX_LIGHTS * (activePage))}
        >
          <PaginationLink>
            {(activePage + 1).toString()}
          </PaginationLink>
        </PaginationItem>
      }
      {
        activePage + 2 < lastPage &&
        <PaginationItem disabled>
          <PaginationLink>
            ...
          </PaginationLink>
        </PaginationItem>
      }
      {
        activePage !== lastPage &&
        <PaginationItem
          disabled={activePage + 1 < lastPage}
          onClick={() => {
            if (activePage + 1 < lastPage) return;
            setLightsOffset(MAX_LIGHTS * (lastPage - 1));
          }}
        >
          <PaginationLink>
            {lastPage.toString()}
          </PaginationLink>
        </PaginationItem>
      }
      <PaginationItem
        disabled={activePage === lastPage}
        onClick={() => {
          if (activePage === lastPage) return;
          setLightsOffset(MAX_LIGHTS * (activePage));
        }}
      >
        <PaginationLink next />
      </PaginationItem>
    </Pagination>
  );
};
Pager.propTypes = {
  lightsOffset: PropTypes.number.isRequired,
  setLightsOffset: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
};
export default connect(
  state => ({
    lightsOffset: fromLightsOffset.getLightsOffset(state),
  }), {
    setLightsOffset: fromLightsOffset.setLightsOffset,
  },
)(Pager);

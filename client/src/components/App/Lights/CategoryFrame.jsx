import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import * as fromOpenCategory from '../../../ducks/openCategory';

const CategoryFrame = ({ id, children, isOpenCategory, toggleOpenCategory }) => (
  <div>
    <div>
      <h3>
        {id}
        <Button color="primary" onClick={() => toggleOpenCategory(id)}>Toggle</Button>
      </h3>
    </div>
    {isOpenCategory && <div>{children}</div>}
  </div>
);
CategoryFrame.propTypes = {
  children: PropTypes.node.isRequired,
  id: PropTypes.string.isRequired,
  isOpenCategory: PropTypes.bool.isRequired,
  toggleOpenCategory: PropTypes.func.isRequired,
};
export default connect(
  (state, { id }) => ({
    isOpenCategory: fromOpenCategory.getIsOpenCategory(state, id),
  }), {
    toggleOpenCategory: fromOpenCategory.toggleOpenCategory,
  },
)(CategoryFrame);

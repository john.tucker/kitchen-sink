import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { graphql } from 'react-apollo';
import { Button } from 'reactstrap';
import { LIGHTS_GQL, FACET_CATEGORY_BYID, FACET_CATEGORY_IDS, MAX_LIGHTS } from '../../../strings';
import * as fromSelectedFilters from '../../../ducks/selectedFilters';
import * as fromLightsOffset from '../../../ducks/lightsOffset';
import Error from './Error';
import StringFilter from './StringFilter';
import Frame from './Frame';
import LoadedFrame from './LoadedFrame';
import Loading from './Loading';
import SelectedFiltersFrame from './SelectedFiltersFrame';
import SelectedStringFilter from './SelectedStringFilter';
import Light from './Light';
import LightsFrame from './LightsFrame';
import LongFilter from './LongFilter';
import SelectedLongFilter from './SelectedLongFilter';
import CategoryFrame from './CategoryFrame';
import Pager from './Pager';

const Lights = ({
  clearSelectedFilters,
  data: { error, loading, lights },
  selectedFilters,
}) => {
  let lightsData = null;
  let stringFilters = null;
  let longFilters = null;
  let totalCount = null;
  const stringFilterById = {};
  const longFilterById = {};
  const facetCategoryIds = [];
  const facetCategoryById = {};
  if (lights !== undefined) {
    totalCount = lights.totalCount;
    lightsData = lights.lights;
    stringFilters = lights.filters.strings;
    longFilters = lights.filters.longs;
    // STRING FILTER LOOKUP
    for (let i = 0; i < stringFilters.length; i += 1) {
      const filter = stringFilters[i];
      stringFilterById[filter.name] = filter;
    }
    // LONG FILTER LOOKUP
    for (let i = 0; i < longFilters.length; i += 1) {
      const filter = longFilters[i];
      longFilterById[filter.name] = filter;
    }
    // DETERMINE WHICH CATEGORIES AND FACETS TO SHOW BASED ON FILTERS
    for (let i = 0; i < FACET_CATEGORY_IDS.length; i += 1) {
      const facets = [];
      const categoryId = FACET_CATEGORY_IDS[i];
      const category = FACET_CATEGORY_BYID[categoryId];
      for (let j = 0; j < category.length; j += 1) {
        const facet = category[j];
        if (facet.type === 'string' && stringFilterById[facet.id] !== undefined) {
          facets.push(facet);
        }
        if (facet.type === 'long' && longFilterById[facet.id] !== undefined) {
          facets.push(facet);
        }
      }
      if (facets.length !== 0) {
        facetCategoryIds.push(categoryId);
        facetCategoryById[categoryId] = facets;
      }
    }
  }
  return (
    <Frame>
      { error !== undefined && <Error /> }
      { error === undefined && loading && <Loading /> }
      { error === undefined && !loading &&
        <LoadedFrame>
          {(
            selectedFilters.length !== 0
          ) &&
            <Button
              color="primary"
              onClick={() => {
                clearSelectedFilters();
              }}
            >Clear All</Button>
          }
          {
            facetCategoryIds.map(id => (
              <CategoryFrame
                key={id}
                id={id}
              >
                {facetCategoryById[id].map(facet => (
                  facet.type === 'string' ?
                    <StringFilter
                      key={facet.id}
                      name={facet.id}
                      options={stringFilterById[facet.id].options}
                    /> :
                    <LongFilter
                      key={facet.id}
                      name={facet.id}
                      max={longFilterById[facet.id].max}
                      min={longFilterById[facet.id].min}
                    />
                ))}
              </CategoryFrame>
            ))
          }
          <SelectedFiltersFrame>
            {selectedFilters.map(filter => (
              filter.type === 'string' ?
                <SelectedStringFilter
                  key={`${filter.filter.name}-${filter.filter.option}`}
                  filterName={filter.filter.name}
                  option={filter.filter.option}
                /> :
                <SelectedLongFilter
                  key={filter.filter.name}
                  name={filter.filter.name}
                  min={filter.filter.min}
                  max={filter.filter.max}
                />
            ))}
          </SelectedFiltersFrame>
          <LightsFrame>
            {lightsData.map(light => (
              <Light
                key={light.id}
                id={light.id}
                mainPhoto={light.mainPhoto}
                polar={light.polar}
              />
            ))}
          </LightsFrame>
          <Pager
            totalCount={totalCount}
          />
        </LoadedFrame>
      }
    </Frame>
  );
};
Lights.propTypes = {
  clearSelectedFilters: PropTypes.func.isRequired,
  data: PropTypes.shape({
    error: PropTypes.obj,
    loading: PropTypes.bool.isRequired,
    lights: PropTypes.shape({
      totalCount: PropTypes.number.isRequired,
      filters: PropTypes.shape({
        strings: PropTypes.arrayOf(PropTypes.shape({
          name: PropTypes.string.isRequired,
        })),
        longs: PropTypes.arrayOf(PropTypes.shape({
          name: PropTypes.string.isRequired,
        })),
      }).isRequired,
      lights: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        mainPhoto: PropTypes.string.isRequired,
        polar: PropTypes.string.isRequired,
      })).isRequired,
    }),
  }).isRequired,
  selectedFilters: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.string.isRequired,
    filter: PropTypes.object.isRequired,
  })).isRequired,
};
const AppGraphQL = graphql(
  LIGHTS_GQL,
  {
    options: ({ lightsOffset, selectedLongFilters, selectedStringFilters }) => {
      const inputStringIds = [];
      const inputStringById = {};
      for (let i = 0; i < selectedStringFilters.length; i += 1) {
        const selectedStringFilter = selectedStringFilters[i];
        if (inputStringById[selectedStringFilter.name] === undefined) {
          inputStringIds.push(selectedStringFilter.name);
          inputStringById[selectedStringFilter.name] = {
            name: selectedStringFilter.name,
            options: [selectedStringFilter.option],
          };
        } else {
          inputStringById[selectedStringFilter.name].options.push(selectedStringFilter.option);
        }
      }
      return ({
        variables: {
          input: {
            first: MAX_LIGHTS,
            offset: lightsOffset,
            strings: inputStringIds.map(o => inputStringById[o]),
            longs: selectedLongFilters,
          },
        },
      });
    },
  },
)(Lights);
export default connect(
  state => ({
    lightsOffset: fromLightsOffset.getLightsOffset(state),
    selectedFilters: fromSelectedFilters.getSelectedFilters(state),
    selectedStringFilters: fromSelectedFilters.getSelectedStringFilters(state),
    selectedLongFilters: fromSelectedFilters.getSelectedLongFilters(state),
  }), {
    clearSelectedFilters: fromSelectedFilters.clearSelectedFilters,
  },
)(AppGraphQL);

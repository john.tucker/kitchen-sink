import React from 'react';
import { PropTypes } from 'prop-types';
import { Button } from 'reactstrap';
import { DISPLAY_NAME_BY_ID, LONG_FILTER_PRECISION_BY_ID } from '../../../../strings';

const View = ({
  max,
  min,
  name,
  removeSelectedLongFilter,
}) => {
  let viewMin = null;
  let viewMax = null;
  const precision = LONG_FILTER_PRECISION_BY_ID[name];
  if (precision !== undefined) {
    // eslint-disable-next-line
    viewMin = (min / Math.pow(10, precision)).toFixed(precision);
    // eslint-disable-next-line
    viewMax = (max / Math.pow(10, precision)).toFixed(precision);
  } else {
    viewMin = min.toString();
    viewMax = max.toString();
  }
  return (
    <Button
      color="primary"
      onClick={() => removeSelectedLongFilter({
        name,
        min,
        max,
      })}
    >{DISPLAY_NAME_BY_ID[name]}: {viewMin} - {viewMax}</Button>
  );
};
View.propTypes = {
  max: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  removeSelectedLongFilter: PropTypes.func.isRequired,
};
export default View;

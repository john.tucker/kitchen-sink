import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import * as fromSelectedFilters from '../../../../ducks/selectedFilters';
import View from './View';

const SelectedLongFilter = ({ max, min, name, removeSelectedLongFilter }) => (
  <View
    max={max}
    min={min}
    name={name}
    removeSelectedLongFilter={removeSelectedLongFilter}
  />
);
SelectedLongFilter.propTypes = {
  max: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  removeSelectedLongFilter: PropTypes.func.isRequired,
};
export default connect(
  null, {
    removeSelectedLongFilter: fromSelectedFilters.removeSelectedLongFilter,
  },
)(SelectedLongFilter);

import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import * as fromSelectedFilters from '../../../../../ducks/selectedFilters';
import View from './View';

const Option = ({
  addSelectedStringFilter,
  filterName,
  option,
  removeSelectedStringFilter,
  selectedStringFilters,
}) => {
  const selected = selectedStringFilters
    .find(filter => (
      filter.name === filterName &&
      filter.option === option
    )) !== undefined;
  return (
    <View
      addSelectedStringFilter={addSelectedStringFilter}
      filterName={filterName}
      option={option}
      removeSelectedStringFilter={removeSelectedStringFilter}
      selected={selected}
    />
  );
};
Option.propTypes = {
  addSelectedStringFilter: PropTypes.func.isRequired,
  filterName: PropTypes.string.isRequired,
  option: PropTypes.string.isRequired,
  removeSelectedStringFilter: PropTypes.func.isRequired,
  selectedStringFilters: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    option: PropTypes.string.isRequired,
  })).isRequired,
};
export default connect(
  state => ({
    selectedStringFilters: fromSelectedFilters.getSelectedStringFilters(state),
  }),
  {
    addSelectedStringFilter: fromSelectedFilters.addSelectedStringFilter,
    removeSelectedStringFilter: fromSelectedFilters.removeSelectedStringFilter,
  },
)(Option);

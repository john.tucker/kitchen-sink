import React from 'react';
import { PropTypes } from 'prop-types';

const View = ({
  addSelectedStringFilter,
  filterName,
  option,
  removeSelectedStringFilter,
  selected,
}) => (
  <div>
    <input
      type="checkbox"
      checked={selected}
      onChange={() => {
        if (!selected) {
          addSelectedStringFilter({ name: filterName, option });
        } else {
          removeSelectedStringFilter({ name: filterName, option });
        }
      }}
    /> {option}
  </div>
);
View.propTypes = {
  addSelectedStringFilter: PropTypes.func.isRequired,
  filterName: PropTypes.string.isRequired,
  option: PropTypes.string.isRequired,
  removeSelectedStringFilter: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
};
export default View;

import React from 'react';
import { PropTypes } from 'prop-types';

const Frame = ({ children, name }) => (
  <div>
    <div><b>{ name }</b></div>
    <div>{children}</div>
  </div>
);
Frame.propTypes = {
  children: PropTypes.node.isRequired,
  name: PropTypes.string.isRequired,
};
export default Frame;

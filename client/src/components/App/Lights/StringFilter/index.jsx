import React from 'react';
import { PropTypes } from 'prop-types';
import Frame from './Frame';
import Option from './Option';
import { DISPLAY_NAME_BY_ID } from '../../../../strings';

const StringFilter = ({ name, options }) => (
  <Frame name={DISPLAY_NAME_BY_ID[name]}>
    {options.map(option => (
      <Option
        key={option}
        filterName={name}
        option={option}
      />
    ))}
  </Frame>
);
StringFilter.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
};
export default StringFilter;

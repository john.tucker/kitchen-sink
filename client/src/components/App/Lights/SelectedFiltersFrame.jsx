import React from 'react';
import { PropTypes } from 'prop-types';

const SelectedFiltersFrame = ({ children }) => (
  <div>
    <div><h3>Selected Filters</h3></div>
    <div>{children}</div>
  </div>
);
SelectedFiltersFrame.propTypes = {
  children: PropTypes.node.isRequired,
};
export default SelectedFiltersFrame;

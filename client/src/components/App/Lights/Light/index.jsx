import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './index.css';

const Light = ({ id, mainPhoto, polar }) => (
  <div>
    <li>{id}</li>
    <div
      style={{ backgroundImage: `url(${mainPhoto})` }}
      className={styles.rootImage}
    />
    <div
      style={{ backgroundImage: `url(${polar})` }}
      className={styles.rootImage}
    />
  </div>
);
Light.propTypes = {
  id: PropTypes.string.isRequired,
  mainPhoto: PropTypes.string.isRequired,
  polar: PropTypes.string.isRequired,
};
export default Light;

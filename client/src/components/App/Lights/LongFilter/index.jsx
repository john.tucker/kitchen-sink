import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import * as fromSelectedFilters from '../../../../ducks/selectedFilters';
import Frame from './Frame';
import { DISPLAY_NAME_BY_ID, LONG_FILTER_PRECISION_BY_ID } from '../../../../strings';

// TEMP IMPLEMENTATION
class LongFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputMin: '0',
      inputMax: '0',
    };
  }
  render() {
    const {
      addSelectedLongFilter,
      max,
      min,
      name,
      removeSelectedLongFilter,
      selectedLongFilters,
    } = this.props;
    const {
      inputMin,
      inputMax,
    } = this.state;
    const filter = selectedLongFilters.find(o => o.name === name);
    let viewMin = null;
    let viewMax = null;
    const precision = LONG_FILTER_PRECISION_BY_ID[name];
    if (precision !== undefined) {
      // eslint-disable-next-line
      viewMin = (min / Math.pow(10, precision)).toFixed(precision);
      // eslint-disable-next-line
      viewMax = (max / Math.pow(10, precision)).toFixed(precision);
    } else {
      viewMin = min.toString();
      viewMax = max.toString();
    }
    return (
      <Frame name={DISPLAY_NAME_BY_ID[name]}>
        <div>
          <div>min: {viewMin}</div>
          <div>max: {viewMax}</div>
        </div>
        {filter !== undefined ?
          <div>
            <button
              onClick={() => {
                removeSelectedLongFilter({
                  name,
                  min: filter.min,
                  max: filter.max,
                });
              }}
            >Unset</button>
          </div> :
          <div>
            <input
              value={inputMin}
              onChange={event => this.setState({
                inputMin: event.target.value,
                inputMax,
              })}
            />
            <input
              value={inputMax}
              onChange={event => this.setState({
                inputMin,
                inputMax: event.target.value,
              })}
            />
            <div>
              <button
                onClick={() => {
                  addSelectedLongFilter({
                    name,
                    min: parseFloat(inputMin) *
                    // eslint-disable-next-line
                      (precision !== undefined ? Math.pow(10, precision) : 1),
                    max: parseFloat(inputMax) *
                    // eslint-disable-next-line
                     (precision !== undefined ? Math.pow(10, precision) : 1),
                  });
                  this.setState({
                    inputMin: '0',
                    inputMax: '0',
                  });
                }}
              >
                Set
              </button>
            </div>
          </div>
        }
      </Frame>
    );
  }
}
LongFilter.propTypes = {
  addSelectedLongFilter: PropTypes.func.isRequired,
  max: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  removeSelectedLongFilter: PropTypes.func.isRequired,
  // eslint-disable-next-line
  selectedLongFilters: PropTypes.array.isRequired,
};
export default connect(
  state => ({
    selectedLongFilters: fromSelectedFilters.getSelectedLongFilters(state),
  }),
  {
    addSelectedLongFilter: fromSelectedFilters.addSelectedLongFilter,
    removeSelectedLongFilter: fromSelectedFilters.removeSelectedLongFilter,
  },
)(LongFilter);

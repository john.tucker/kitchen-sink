import React from 'react';
import { PropTypes } from 'prop-types';

const LoadedFrame = ({ children }) => (
  <div>{children}</div>
);
LoadedFrame.propTypes = {
  children: PropTypes.node.isRequired,
};
export default LoadedFrame;

import React from 'react';
import { PropTypes } from 'prop-types';

const LightsFrame = ({ children }) => (
  <div>
    <div><h3>Lights</h3></div>
    <ul>{children}</ul>
  </div>
);
LightsFrame.propTypes = {
  children: PropTypes.node.isRequired,
};
export default LightsFrame;

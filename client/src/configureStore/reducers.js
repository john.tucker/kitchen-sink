import { combineReducers } from 'redux';
import lightsOffset from '../ducks/lightsOffset';
import openCategory from '../ducks/openCategory';
import selectedFilters from '../ducks/selectedFilters';

const reducers = {
  lightsOffset,
  openCategory,
  selectedFilters,
};
export default combineReducers(reducers);

import { combineActions, createAction, handleActions } from 'redux-actions';
import { ACTION_PREFIX } from '../strings';
import {
  addSelectedStringFilter,
  removeSelectedStringFilter,
  addSelectedLongFilter,
  removeSelectedLongFilter,
  clearSelectedFilters,
} from './selectedFilters';

export const setLightsOffset = createAction(`${ACTION_PREFIX}SET_LIGHTS_OFFSET`);
export default handleActions({
  [setLightsOffset](_, action) {
    return action.payload;
  },
  [combineActions(
    addSelectedStringFilter,
    removeSelectedStringFilter,
    addSelectedLongFilter,
    removeSelectedLongFilter,
    clearSelectedFilters,
  )]() {
    return 0;
  },
}, 0);
export const getLightsOffset = state => state.lightsOffset;

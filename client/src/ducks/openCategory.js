import { createAction, handleActions } from 'redux-actions';
import { ACTION_PREFIX } from '../strings';

export const toggleOpenCategory = createAction(`${ACTION_PREFIX}TOGGLE_CATEGORY`);
export default handleActions({
  [toggleOpenCategory](state, action) {
    const category = action.payload;
    if (state[category] === undefined) {
      return { ...state, ...{ [category]: true } };
    }
    const newState = { ...state };
    delete newState[category];
    return newState;
  },
}, {});
export const getIsOpenCategory =
  (state, category) => state.openCategory[category] !== undefined;

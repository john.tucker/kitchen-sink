import { createAction, handleActions } from 'redux-actions';
import { ACTION_PREFIX } from '../strings';

export const addSelectedStringFilter = createAction(`${ACTION_PREFIX}ADD_SELECTED_STRING_FILTER`);
export const removeSelectedStringFilter = createAction(`${ACTION_PREFIX}REMOVE_SELECTED_STRING_FILTER`);
export const addSelectedLongFilter = createAction(`${ACTION_PREFIX}ADD_SELECTED_LONG_FILTER`);
export const removeSelectedLongFilter = createAction(`${ACTION_PREFIX}REMOVE_SELECTED_LONG_FILTER`);
export const clearSelectedFilters = createAction(`${ACTION_PREFIX}CLEAR_SELECTED_FILTERS`);
export default handleActions({
  [addSelectedStringFilter](state, action) {
    return [...state, { type: 'string', filter: action.payload }];
  },
  [removeSelectedStringFilter](state, action) {
    const newState = [...state];
    newState.splice(state.findIndex(
      o => (o.filter.name === action.payload.name && o.filter.option === action.payload.option),
    ), 1);
    return newState;
  },
  [addSelectedLongFilter](state, action) {
    return [...state, { type: 'long', filter: action.payload }];
  },
  [removeSelectedLongFilter](state, action) {
    const newState = [...state];
    newState.splice(state.findIndex(
      o => (o.filter.name === action.payload.name),
    ), 1);
    return newState;
  },
  [clearSelectedFilters]() {
    return [];
  },
}, []);
export const getSelectedFilters = state => state.selectedFilters;
export const getSelectedStringFilters = state => state.selectedFilters
  .filter(filter => filter.type === 'string')
  .map(filter => filter.filter);
export const getSelectedLongFilters = state => state.selectedFilters
  .filter(filter => filter.type === 'long')
  .map(filter => filter.filter);

import { gql } from 'react-apollo';

export const ACTION_PREFIX = 'app/';
export const ENDPOINT = 'https://us-central1-kitchen-sink-staging.cloudfunctions.net/api/graphql';
export const MAX_LIGHTS = 10;
export const LIGHTS_GQL = gql`
query lightsWithInput($input: FilterInput!) {
  lights(input: $input) {
    totalCount
    lights {
      id
      mainPhoto
      polar
    }
    filters {
      strings {
        name
        options
      }
      longs {
        name
        min
        max
      }
    }
  }
}
`;
export const LONG_FILTER_PRECISION_BY_ID = {
  housingWidth: 2,
  housingHeight: 2,
  housingLength: 2,
  wattage: 2,
  totalLuminaireWatts: 2,
  spacingCriterion0180: 2,
  spacingCriterion90270: 2,
  spacingCriterionDiagonal: 2,
  luminousLength0180: 2,
  luminousWidth90270: 2,
  luminousHeight: 2,
};
export const DISPLAY_NAME_BY_ID = {
  mainPhoto: 'Main Photo',
  applicationPhotos: 'Application Photos',
  cutsheet: 'Cutsheet',
  bim: 'BIM',
  instructions: 'Instructions',
  photometry: 'Photometry',
  contact: 'Contact',
  lightingRepresentative: 'Lighting Representative',
  polar: 'Polar',
  manufacturer: 'Manufacturer',
  series: 'Series',
  style: 'Style',
  shape: 'Shape',
  housingMaterial: 'Housing Material',
  housingFinish: 'Housing Finish',
  reflectorMaterial: 'Reflector Material',
  reflectorFinish: 'Reflector Finish',
  opticalMaterial: 'Optic Material',
  opticalFinish: 'Optic Finish',
  lightDistribution: 'Light Distribution',
  macAdamEllipseSDCMSteps: 'Mac Adam Ellipse SDCM Steps',
  deliveredLumens: 'Delivered Lumens',
  lightSource: 'Light Source',
  lampBase: 'Lamp Base',
  electrical: 'Electrical',
  driver: 'Driver',
  labels: 'Labels',
  location: 'Location',
  buildingType: 'Building Type',
  roomType: 'Room Type',
  interior: 'Interior',
  interiorDetail: 'Interior Detail',
  warranty: 'Warranty',
  cieType: 'CIE Type',
  basicLuminousShape: 'Basic Luminous Shape',
  housingWidth: 'Housing Width',
  housingHeight: 'Housing Height',
  housingLength: 'Housing Length',
  colorTemperature: 'Color Temperature',
  cri: 'CRI',
  lumensPerLinearFoot: 'Lumens Per Linear Foot',
  wattage: 'Wattage',
  luminaireLumens: 'Luminaire Lumens',
  luminaireEfficacyRating: 'Luminaire Efficacy Rating (LER)',
  totalLuminaireWatts: 'Total Luminaire Watts',
  ballastFactor: 'Ballast Factor',
  spacingCriterion0180: 'Spacing Criterion (0-180)',
  spacingCriterion90270: 'Spacing Criterion (90-270)',
  spacingCriterionDiagonal: 'Spacing Criterion (Diagonal)',
  luminousLength0180: 'Luminous Length (0-180)',
  luminousWidth90270: 'Luminous Width (90-270)',
  luminousHeight: 'Luminous Height',
};
export const FACET_CATEGORY_IDS = [
  'Manufacturer',
  'Style',
  'Shape',
  'Housing Construction',
  'Housing Dimensions',
  'Reflector Information',
  'Optic Information',
  'Illumination Information',
  'Electrical',
  'Driver',
  'Certifications & Listings',
  'Application',
  'Mounting',
  'Warranty',
  'Indoor Report',
];
export const FACET_CATEGORY_BYID = {
  Manufacturer: [
    { id: 'manufacturer', type: 'string' },
    { id: 'series', type: 'string' },
  ],
  Style: [
    { id: 'style', type: 'string' },
  ],
  Shape: [
    { id: 'shape', type: 'string' },
  ],
  'Housing Construction': [
    { id: 'housingMaterial', type: 'string' },
    { id: 'housingFinish', type: 'string' },
  ],
  'Housing Dimensions': [
    { id: 'housingWidth', type: 'long' },
    { id: 'housingHeight', type: 'long' },
    { id: 'housingLength', type: 'long' },
  ],
  'Reflector Information': [
    { id: 'reflectorMaterial', type: 'string' },
    { id: 'reflectorFinish', type: 'string' },
  ],
  'Optic Information': [
    { id: 'opticalMaterial', type: 'string' },
    { id: 'opticalFinish', type: 'string' },
  ],
  'Illumination Information': [
    { id: 'lightDistribution', type: 'string' },
    { id: 'colorTemperature', type: 'long' },
    { id: 'cri', type: 'long' },
    { id: 'macAdamEllipseSDCMSteps', type: 'string' },
    { id: 'lumensPerLinearFoot', type: 'long' },
    { id: 'deliveredLumens', type: 'string' },
    { id: 'lightSource', type: 'string' },
    { id: 'lampBase', type: 'string' },
  ],
  Electrical: [
    { id: 'electrical', type: 'string' },
    { id: 'wattage', type: 'long' },
  ],
  Driver: [
    { id: 'driver', type: 'string' },
  ],
  'Certifications & Listings': [
    { id: 'labels', type: 'string' },
  ],
  Application: [
    { id: 'location', type: 'string' },
    { id: 'buildingType', type: 'string' },
    { id: 'roomType', type: 'string' },
  ],
  Mounting: [
    { id: 'interior', type: 'string' },
    { id: 'interiorDetail', type: 'string' },
  ],
  Warranty: [
    { id: 'warranty', type: 'string' },
  ],
  'Indoor Report': [
    { id: 'luminaireLumens', type: 'long' },
    { id: 'luminaireEfficacyRating', type: 'long' },
    { id: 'totalLuminaireWatts', type: 'long' },
    { id: 'ballastFactor', type: 'long' },
    { id: 'cieType', type: 'string' },
    { id: 'spacingCriterion0180', type: 'long' },
    { id: 'spacingCriterion90270', type: 'long' },
    { id: 'spacingCriterionDiagonal', type: 'long' },
    { id: 'basicLuminousShape', type: 'string' },
    { id: 'luminousLength0180', type: 'long' },
    { id: 'luminousWidth90270', type: 'long' },
    { id: 'luminousHeight', type: 'long' },
  ],
};

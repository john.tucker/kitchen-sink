const fetch = require('node-fetch');

const ARRAY_PROPERTIES = ['applicationPhotos'];
const ARRAY_FACETS = ['labels'];
const DBL_ERROR_MSG = '400: Double quotes not allowed in inputs';
const NET_ERROR_MSG = '500: Database unavailable';
// TODO: REMOVE WHEN CONVERT
const URL = 'https://search-demo-zvrregattfyeqnrsj2daofrgqa.us-east-1.es.amazonaws.com/lights/light/_search';
const URL2 = 'https://search-demo-zvrregattfyeqnrsj2daofrgqa.us-east-1.es.amazonaws.com/light/light/_search';
const QUERY_HEADER =
`{
  "size": 0,
  "query": {
    "bool": {
      "filter": [`;

const QUERY_FOOTER_BOTH =
`     ]
    }
  },
  "aggs": {
    "results": {
      "terms": {
        "field": "result",
        "order" : { "_term" : "asc" }
      }
    },
    "agg_keyword_facet": {
      "nested": {
        "path": "keyword_facets"
      },
      "aggs": {
        "facet_name": {
          "terms": {
            "field": "keyword_facets.facet_name",
            "size": 30
          },
          "aggs": {
            "facet_value": {
              "terms": {
                "field": "keyword_facets.facet_value"
              }
            }
          }
        }
      }
    },
    "agg_long_facet": {
      "nested": {
        "path": "long_facets"
        },
        "aggs": {
          "facet_name" : {
            "terms": {
              "field": "long_facets.facet_name",
              "size": 30
            },
            "aggs": {
              "facet_value": {
                "stats": {
                  "field": "long_facets.facet_value"
                }
              }
            }
          }
        }
      }
    }
  }
}`;
const QUERY_FOOTER_KEYWORD =
`     ]
    }
  },
  "aggs": {
    "agg_keyword_facet": {
      "nested": {
        "path": "keyword_facets"
      },
      "aggs": {
        "facet_name": {
          "terms": {
            "field": "keyword_facets.facet_name",
            "size": 30
          },
          "aggs": {
            "facet_value": {
              "terms": {
                "field": "keyword_facets.facet_value"
              }
            }
          }
        }
      }
    }
  }
}`;
const QUERY_FOOTER_LONG =
`     ]
    }
  },
  "aggs": {
    "agg_long_facet": {
      "nested": {
        "path": "long_facets"
        },
        "aggs": {
          "facet_name" : {
            "terms": {
              "field": "long_facets.facet_name",
              "size": 30
            },
            "aggs": {
              "facet_value": {
                "stats": {
                  "field": "long_facets.facet_value"
                }
              }
            }
          }
        }
     }
   }
  }
}`;
const GROUP_HEADER =
`{
  "bool": {
    "should": [`;
const GROUP_FOOTER =
`    ]
  }
}`;
const keywordFacetClause = (name, value, last) => (
  // eslint-disable-next-line
`{
  "nested": {
    "path": "keyword_facets",
    "query": {
      "bool": {
        "filter": [
          { "term": { "keyword_facets.facet_name": "${name}" } },
          { "term": { "keyword_facets.facet_value": "${value}" } }
        ]
      }
    }
  }
}${!last ? ', ' : ''}`);
const keywordGroupFacetClause = (name, group, last) => (
  // eslint-disable-next-line
`${GROUP_HEADER}
${group.map((o, i) => keywordFacetClause(name, o, i === group.length - 1)).join('')}
${GROUP_FOOTER}${!last ? ', ' : ''}`
);
const longFacetClause = (name, min, max, last) => (
  // eslint-disable-next-line
`{
  "nested": {
    "path": "long_facets",
    "query": {
      "bool": {
        "filter": [
          { "term": { "long_facets.facet_name": "${name}" } },
          { "range": { "long_facets.facet_value": {
            "gte": ${min},
            "lte": ${max}
          } } }
        ]
      }
    }
  }
}${!last ? ', ' : ''}`);
module.exports = {
  Query: {
    // TODO: REMOVE ONCE CONVERT
    lights(_, { input }) {
      // FIRST AND OFFSET
      const queryHeaderBoth =
      `{
        "from": ${input.offset},
        "size": ${input.first},
        "query": {
          "bool": {
            "filter": [`;
      // STRING FACETS
      const stringFacetIds = input.strings.map(o => o.name);
      const stringFacetById = {};
      for (let i = 0; i < stringFacetIds.length; i += 1) {
        const facetId = stringFacetIds[i];
        stringFacetById[facetId] = input.strings[i];
      }
      // GUARD CLAUSE FOR DOUBLE-QUOTES
      for (let i = 0 ; i < stringFacetIds.length; i += 1) {
        const facetId = stringFacetIds[i];
        for (let j = 0; j < stringFacetById[facetId].options.length; j += 1) {
          if (stringFacetById[facetId].options[j].includes('"')) return Promise.reject(DBL_ERROR_MSG);
        }
      }
      // LONG FACETS
      const longFacetIds = input.longs.map(o => o.name);
      const longFacetById = {};
      for (let i = 0; i < longFacetIds.length; i += 1) {
        const facetId = longFacetIds[i];
        longFacetById[facetId] = input.longs[i];
      }
      const queryBoth = (stringQueryFacets, longQueryFacets) => (
// eslint-disable-next-line
`${queryHeaderBoth}
${stringQueryFacets.map((o, i) => `${keywordGroupFacetClause(o, stringFacetById[o].options, longQueryFacets.length === 0 && i === stringQueryFacets.length - 1)}`).join('')}
${longQueryFacets.map((o, i) => `${longFacetClause(o, longFacetById[o].min, longFacetById[o].max, i === longQueryFacets.length - 1)}`).join('')}
${QUERY_FOOTER_BOTH}`
      );
      const queryKeyword = (stringQueryFacets, longQueryFacets) => (
// eslint-disable-next-line
`${QUERY_HEADER}
${stringQueryFacets.map((o, i) => `${keywordGroupFacetClause(o, stringFacetById[o].options, longQueryFacets.length === 0 && i === stringQueryFacets.length - 1)}`).join('')}
${longQueryFacets.map((o, i) => `${longFacetClause(o, longFacetById[o].min, longFacetById[o].max, i === longQueryFacets.length - 1)}`).join('')}
${QUERY_FOOTER_KEYWORD}`
      );
      const queryLong = (stringQueryFacets, longQueryFacets) => (
// eslint-disable-next-line
`${QUERY_HEADER}
${stringQueryFacets.map((o, i) => `${keywordGroupFacetClause(o, stringFacetById[o].options, longQueryFacets.length === 0 && i === stringQueryFacets.length - 1)}`).join('')}
${longQueryFacets.map((o, i) => `${longFacetClause(o, longFacetById[o].min, longFacetById[o].max, i === longQueryFacets.length - 1)}`).join('')}
${QUERY_FOOTER_LONG}`
      );
      // FIRST QUERY FOR LightS AND NON-FACET FILTERS
      // N QUERIES FOR N FACET FILTERS
      return Promise.all([
        fetch(URL, { method: 'POST', body: queryBoth(stringFacetIds, longFacetIds)}).then(res => res.json()),
        ...stringFacetIds.map(o => fetch(URL, { method: 'POST', body: queryKeyword(stringFacetIds.filter(ob => ob !== o), longFacetIds)}).then(res => res.json())),
        ...longFacetIds.map(o => fetch(URL, { method: 'POST', body: queryLong(stringFacetIds, longFacetIds.filter(ob => ob !== o))}).then(res => res.json()))
      ])
        .then(jsonArray => {
          // Total Count
          const totalCount = jsonArray[0].hits.total;
          // Lights
          const lights = jsonArray[0].hits.hits.map(o => {
            const light = {
              id: o._id,
            };
            Object.assign(light, o._source.entity);
            // ARRAY PROPERTIES FORCE
            for (let i = 0; i < ARRAY_PROPERTIES.length; i += 1) {
              const property = ARRAY_PROPERTIES[i];
              const value = light[property];
              if (!Array.isArray(value)) {
                light[property] = [ value ];
              }
            }
            // STRING FACETS
            for (let i = 0; i < o._source.keyword_facets.length; i += 1) {
              const facet = o._source.keyword_facets[i];
              if (ARRAY_FACETS.includes(facet.facet_name)) {
                if (light[facet.facet_name] === undefined) {
                  light[facet.facet_name] = [ facet.facet_value ];
                } else {
                  light[facet.facet_name].push(facet.facet_value);
                }
              } else {
                light[facet.facet_name] = facet.facet_value;
              }
            }
            // LONG FACETS
            for (let i = 0; i < o._source.long_facets.length; i += 1) {
              const facet = o._source.long_facets[i];
              light[facet.facet_name] = facet.facet_value;
            }
            return light;
          });
          // NON-FACET STRING FILTERS
          const stringFilterIds = [];
          const stringFilterById = {};
          const firstStringBuckets = jsonArray[0].aggregations.agg_keyword_facet.facet_name.buckets;
          for (let i = 0; i < firstStringBuckets.length; i += 1 ) {
            const bucket = firstStringBuckets[i];
            const name = bucket.key;
            const options = bucket.facet_value.buckets.map(ob => ob.key);
            stringFilterIds.push(name);
            stringFilterById[name] = {
              name,
              options,
            };
          }
          // FACET FILTERS OVERRIDE
          for (let i = 0 ; i < stringFacetIds.length; i += 1) {
            const facet = stringFacetIds[i];
            const buckets = jsonArray[i + 1].aggregations.agg_keyword_facet.facet_name.buckets;
            for (let j = 0; j < buckets.length; j += 1 ) {
              const bucket = buckets[j];
              const name = bucket.key;
              const options = bucket.facet_value.buckets.map(ob => ob.key);
              if (name === facet) {
                if (stringFilterById[name] === undefined) stringFilterIds.push(name);
                stringFilterById[name] = {
                  name,
                  options,
                };
              }
            }
          }
          // ENSURE FACET SELECTIONS IN FILTERS
          for (let i = 0; i < stringFacetIds.length; i += 1) {
            const facetId = stringFacetIds[i];
            const options = stringFacetById[facetId].options;
            const filter = stringFilterById[facetId];
            // CASE NO FILTER FOR FACET EXIST
            if (filter === undefined) {
              stringFilterIds.push(facetId);
              stringFilterById[facetId] = {
                name: facetId,
                options,
              };
            } else {
              // CASE FILTER FOR FACET EXISTS
              for (let j = 0; j < options.length; j += 1) {
                const option = options[j];
                if (filter.options.indexOf(option) === -1) filter.options.push(option);
              }
            }
          }
          // NON-FACET LONG FITLERS
          const longFilterIds = [];
          const longFilterById = {};
          const firstLongBuckets = jsonArray[0].aggregations.agg_long_facet.facet_name.buckets;
          for (let i = 0; i < firstLongBuckets.length; i += 1 ) {
            const bucket = firstLongBuckets[i];
            const name = bucket.key;
            longFilterIds.push(name);
            longFilterById[name] = {
              name,
              min: bucket.facet_value.min,
              max: bucket.facet_value.max,
            };
          }
          // FACET LONG FILTERS
          for (let i = 0 ; i < longFacetIds.length; i += 1) {
            const facet = longFacetIds[i];
            const buckets = jsonArray[i + 1 + stringFacetIds.length].aggregations.agg_long_facet.facet_name.buckets;
            for (let j = 0; j < buckets.length; j += 1 ) {
              const bucket = buckets[j];
              const name = bucket.key;
              if (name === facet) {
                if (longFilterById[name] === undefined) longFilterIds.push(name);
                longFilterById[name] = {
                  name,
                  min: Math.min(bucket.facet_value.min, longFacetById[name].min),
                  max: Math.max(bucket.facet_value.max, longFacetById[name].max),
                };
              }
            }
          }
          return ({
            totalCount,
            lights,
            filters: {
              strings: stringFilterIds.map(o => stringFilterById[o]),
              longs: longFilterIds.map(o => longFilterById[o]),
            }
          });
        })
        .catch(() => {
          return NET_ERROR_MSG;
        });
    },
    luminari(_, { input: { first, longs, offset, strings, }}) {
      // STRING FACETS
      const stringFacetIds = strings.map(o => o.name);
      const stringFacetById = {};
      for (let i = 0; i < stringFacetIds.length; i += 1) {
        const facetId = stringFacetIds[i];
        stringFacetById[facetId] = strings[i];
      }
      // GUARD CLAUSE FOR DOUBLE-QUOTES
      for (let i = 0 ; i < stringFacetIds.length; i += 1) {
        const facetId = stringFacetIds[i];
        for (let j = 0; j < stringFacetById[facetId].options.length; j += 1) {
          if (stringFacetById[facetId].options[j].includes('"')) return Promise.reject(DBL_ERROR_MSG);
        }
      }
      // LONG FACETS
      const longFacetIds = longs.map(o => o.name);
      const longFacetById = {};
      for (let i = 0; i < longFacetIds.length; i += 1) {
        const facetId = longFacetIds[i];
        longFacetById[facetId] = longs[i];
      }
      const queryBoth = (stringQueryFacets, longQueryFacets) => (
// eslint-disable-next-line
`${QUERY_HEADER}
${stringQueryFacets.map((o, i) => `${keywordGroupFacetClause(o, stringFacetById[o].options, longQueryFacets.length === 0 && i === stringQueryFacets.length - 1)}`).join('')}
${longQueryFacets.map((o, i) => `${longFacetClause(o, longFacetById[o].min, longFacetById[o].max, i === longQueryFacets.length - 1)}`).join('')}
${QUERY_FOOTER_BOTH}`
      );
      const queryKeyword = (stringQueryFacets, longQueryFacets) => (
// eslint-disable-next-line
`${QUERY_HEADER}
${stringQueryFacets.map((o, i) => `${keywordGroupFacetClause(o, stringFacetById[o].options, longQueryFacets.length === 0 && i === stringQueryFacets.length - 1)}`).join('')}
${longQueryFacets.map((o, i) => `${longFacetClause(o, longFacetById[o].min, longFacetById[o].max, i === longQueryFacets.length - 1)}`).join('')}
${QUERY_FOOTER_KEYWORD}`
      );
      const queryLong = (stringQueryFacets, longQueryFacets) => (
// eslint-disable-next-line
`${QUERY_HEADER}
${stringQueryFacets.map((o, i) => `${keywordGroupFacetClause(o, stringFacetById[o].options, longQueryFacets.length === 0 && i === stringQueryFacets.length - 1)}`).join('')}
${longQueryFacets.map((o, i) => `${longFacetClause(o, longFacetById[o].min, longFacetById[o].max, i === longQueryFacets.length - 1)}`).join('')}
${QUERY_FOOTER_LONG}`
      );
      // FIRST QUERY FOR LUMINARI AND NON-FACET FILTERS
      // N QUERIES FOR N FACET FILTERS
      return Promise.all([
        fetch(URL2, { method: 'POST', body: queryBoth(stringFacetIds, longFacetIds)}).then(res => res.json()),
        ...stringFacetIds.map(o => fetch(URL, { method: 'POST', body: queryKeyword(stringFacetIds.filter(ob => ob !== o), longFacetIds)}).then(res => res.json())),
        ...longFacetIds.map(o => fetch(URL, { method: 'POST', body: queryLong(stringFacetIds, longFacetIds.filter(ob => ob !== o))}).then(res => res.json()))
      ])
        .then(jsonArray => {
          // LUMINARI
          const buckets = jsonArray[0].aggregations.results.buckets;
          const length = buckets.length;
          const luminari = [];
          for (let i = offset; i < Math.min(offset + first, length); i += 1 ) {
            const luminare = buckets[i];
            const parts = luminare.key.split('|');
            luminari.push({
              id: parts[2],
              manufacturer: parts[0],
              series: parts[1],
            });
          }
          // NON-FACET STRING FILTERS
          const stringFilterIds = [];
          const stringFilterById = {};
          const firstStringBuckets = jsonArray[0].aggregations.agg_keyword_facet.facet_name.buckets;
          for (let i = 0; i < firstStringBuckets.length; i += 1 ) {
            const bucket = firstStringBuckets[i];
            const name = bucket.key;
            const options = bucket.facet_value.buckets.map(ob => ob.key);
            stringFilterIds.push(name);
            stringFilterById[name] = {
              name,
              options,
            };
          }
          // FACET FILTERS OVERRIDE
          for (let i = 0 ; i < stringFacetIds.length; i += 1) {
            const facet = stringFacetIds[i];
            const buckets = jsonArray[i + 1].aggregations.agg_keyword_facet.facet_name.buckets;
            for (let j = 0; j < buckets.length; j += 1 ) {
              const bucket = buckets[j];
              const name = bucket.key;
              const options = bucket.facet_value.buckets.map(ob => ob.key);
              if (name === facet) {
                if (stringFilterById[name] === undefined) stringFilterIds.push(name);
                stringFilterById[name] = {
                  name,
                  options,
                };
              }
            }
          }
          // ENSURE FACET SELECTIONS IN FILTERS
          for (let i = 0; i < stringFacetIds.length; i += 1) {
            const facetId = stringFacetIds[i];
            const options = stringFacetById[facetId].options;
            const filter = stringFilterById[facetId];
            // CASE NO FILTER FOR FACET EXIST
            if (filter === undefined) {
              stringFilterIds.push(facetId);
              stringFilterById[facetId] = {
                name: facetId,
                options,
              };
            } else {
              // CASE FILTER FOR FACET EXISTS
              for (let j = 0; j < options.length; j += 1) {
                const option = options[j];
                if (filter.options.indexOf(option) === -1) filter.options.push(option);
              }
            }
          }
          // NON-FACET LONG FITLERS
          const longFilterIds = [];
          const longFilterById = {};
          const firstLongBuckets = jsonArray[0].aggregations.agg_long_facet.facet_name.buckets;
          for (let i = 0; i < firstLongBuckets.length; i += 1 ) {
            const bucket = firstLongBuckets[i];
            const name = bucket.key;
            longFilterIds.push(name);
            longFilterById[name] = {
              name,
              min: bucket.facet_value.min,
              max: bucket.facet_value.max,
            };
          }
          // FACET LONG FILTERS
          for (let i = 0 ; i < longFacetIds.length; i += 1) {
            const facet = longFacetIds[i];
            const buckets = jsonArray[i + 1 + stringFacetIds.length].aggregations.agg_long_facet.facet_name.buckets;
            for (let j = 0; j < buckets.length; j += 1 ) {
              const bucket = buckets[j];
              const name = bucket.key;
              if (name === facet) {
                if (longFilterById[name] === undefined) longFilterIds.push(name);
                longFilterById[name] = {
                  name,
                  min: Math.min(bucket.facet_value.min, longFacetById[name].min),
                  max: Math.max(bucket.facet_value.max, longFacetById[name].max),
                };
              }
            }
          }
          return ({
            isLastPage: offset + first >= length,
            luminari,
            filters: {
              strings: stringFilterIds.map(o => stringFilterById[o]),
              longs: longFilterIds.map(o => longFilterById[o]),
            }
          });
        })
        .catch(() => {
          return NET_ERROR_MSG;
        });
    }
  },
};

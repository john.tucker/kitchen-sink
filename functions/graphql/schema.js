const graphqlTools = require('graphql-tools');
const resolvers = require('./resolvers');

const schema = `
input FilterInput {
  first: Int!,
  offset: Int!,
  strings: [StringFilterInput]!
  longs: [LongFilterInput]!
}
input StringFilterInput {
  name: String!
  options: [String]!
}
input LongFilterInput {
  name: String!
  min: Int!
  max: Int!
}
type Light {
  id: String!
  mainPhoto: String!
  applicationPhotos: [String]!
  cutsheet: String!
  bim: String!
  instructions: String!
  photometry: String!
  contact: String!
  lightingRepresentative: String!
  polar: String!
  manufacturer: String
  series: String
  style: String
  shape: String
  housingMaterial: String
  housingFinish: String
  reflectorMaterial: String
  reflectorFinish: String
  opticalMaterial: String
  opticalFinish: String
  lightDistribution: String
  macAdamEllipseSDCMSteps: String
  deliveredLumens: String
  lightSource: String
  lampBase: String
  electrical: String
  driver: String
  labels: [String]
  location: String
  buildingType: String
  roomType: String
  interior: String
  interiorDetail: String
  warranty: String
  cieType: String
  basicLuminousShape: String
  housingWidth: Int
  housingHeight: Int
  housingLength: Int
  colorTemperature: Int
  cri: Int
  lumensPerLinearFoot: Int
  wattage: Int
  luminaireLumens: Int
  luminaireEfficacyRating: Int
  totalLuminaireWatts: Int
  ballastFactor: Int
  spacingCriterion0180: Int
  spacingCriterion90270: Int
  spacingCriterionDiagonal: Int
  luminousLength0180: Int
  luminousWidth90270: Int
  luminousHeight: Int
}
type StringFilter {
  name: String!
  options: [String]!
}
type LongFilter {
  name: String!
  min: Int!
  max: Int!
}
type Filters {
  strings: [StringFilter]!
  longs: [LongFilter]!
}
type LightsWithFilters {
  totalCount: Int!
  lights: [Light]!
  filters: Filters!
}
input LuminariInput {
  first: Int!
  offset: Int!
  strings: [StringFilterInput]!
  longs: [LongFilterInput]!
}
type Luminare {
  id: String!
  manufacturer: String!
  series: String!
}
type LuminariWithFilters {
  isLastPage: Boolean!
  luminari: [Luminare]!
  filters: Filters!
}
type Query {
  lights(input: FilterInput!): LightsWithFilters
  luminari(input: LuminariInput!): LuminariWithFilters
}
`;
module.exports = graphqlTools.makeExecutableSchema({
  typeDefs: schema,
  resolvers
});
